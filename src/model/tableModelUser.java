/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author ASUS
 */
public class tableModelUser  extends AbstractTableModel{
    
    List<User> lb;

    public tableModelUser(List<User> lb) {
        this.lb = lb;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }


    public int getRowCount() {
        return lb.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "id";
            case 1:
                return "Username";
            case 2:
                return "Password";
            case 3:
                return "Nama";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return lb.get(row).getId();
            case 1:
                return lb.get(row).getUsername();
            case 2:
                return lb.get(row).getPassword();
            case 3:
                return lb.get(row).getNama();
            default:
                return null;
        }
    }
}
