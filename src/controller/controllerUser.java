/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import DAO.daoUser;
import DAOImplement.implementUser;
import model.User;
import model.tableModelUser;
import views.MainView;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author ASUS
 */
public class controllerUser {
    
    MainView frame;
    implementUser implUser;
    List<User> lb;

    public controllerUser(MainView frame) {
        this.frame = frame;
        implUser = new daoUser();
        lb = implUser.getALL();
    }

    //mengosongkan field
    public void reset() {
        frame.getTxtID().setText("");
        frame.getTxtUsername().setText("");
        frame.getTxtNama().setText("");
        frame.getTxtPassword().setText("");
        
    }

    //menampilkan data ke dalam tabel
    public void isiTable() {
        lb = implUser.getALL();
        tableModelUser tmb = new tableModelUser(lb);
        frame.getTabelData().setModel(tmb);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void isiField(int row) {
        frame.getTxtID().setText(lb.get(row).getId().toString());
        frame.getTxtUsername().setText(lb.get(row).getUsername());
        frame.getTxtPassword().setText(lb.get(row).getPassword());
        frame.getTxtNama().setText(lb.get(row).getNama());
    }

    //merupakan fungsi untuk insert data berdasarkan inputan user dari textfield di frame
    public void insert() {
        
      if (!frame.getTxtNama().getText().trim().isEmpty()& !frame.getTxtPassword().getText().trim().isEmpty()) {
          
        User b = new User();
        b.setUsername(frame.getTxtUsername().getText());
        b.setPassword(frame.getTxtPassword().getText());
        b.setNama(frame.getTxtNama().getText());

        implUser.insert(b);
        JOptionPane.showMessageDialog(null, "Simpan Data sukses");
        
        } else {
            JOptionPane.showMessageDialog(frame, "Data Tidak Boleh Kosong");
        }
    }

    //berfungsi untuk update data berdasarkan inputan user dari textfield di frame
    public void update() {
   if (!frame.getTxtID().getText().trim().isEmpty()) {
             
        User b = new User();
        b.setUsername(frame.getTxtUsername().getText());
        b.setPassword(frame.getTxtPassword().getText());
        b.setNama(frame.getTxtNama().getText());
        b.setId(Integer.parseInt(frame.getTxtID().getText()));
        implUser.update(b);
        
        JOptionPane.showMessageDialog(null, "Update Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di ubah");
        }
    }

    //berfungsi menghapus data yang dipilih
    public void delete() {
        if (!frame.getTxtID().getText().trim().isEmpty()) {
            int id = Integer.parseInt(frame.getTxtID().getText());
            implUser.delete(id);
            
            JOptionPane.showMessageDialog(null, "Hapus Data  sukses");
        } else {
            JOptionPane.showMessageDialog(frame, "Pilih data yang akan di hapus");
        }
    }

    public void isiTableCariNama() {
        lb = implUser.getCariNama(frame.getTxtCariNama().getText());
        tableModelUser tmb = new tableModelUser(lb);
        frame.getTabelData().setModel(tmb);
    }

    public void carinama() {
        if (!frame.getTxtCariNama().getText().trim().isEmpty()) {
            implUser.getCariNama(frame.getTxtCariNama().getText());
            isiTableCariNama();
        } else {
            JOptionPane.showMessageDialog(frame, "SILAHKAN PILIH DATA");
        }
    }
}
