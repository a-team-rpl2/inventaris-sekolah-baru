 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import koneksi.koneksi;
import model.User;
import DAOImplement.implementUser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author ASUS
 */
public class daoUser implements implementUser{
    
    Connection connection;
    final String insert = "INSERT INTO tb_user (username, password, nama) VALUES (?, ?,?);";
    final String update = "UPDATE tb_user set username=?, password=?, nama=? where id=? ;";
    final String delete = "DELETE FROM tb_user where id=? ;";
    final String select = "SELECT * FROM tb_user;";
    final String carinama = "SELECT * FROM tb_user where nama like ?";
    
    public daoUser() {
        connection = koneksi.connection();
    }
    
        public List<User>prosesLogin(String user, String pass){
        
        
        List<User> lis = null;
        
        try{
            
          String sql = "select * from tb_user where nama='"+user+"' AND password='"+pass+"'";

          lis = new ArrayList<User>();
          
          Statement stmt=connection.createStatement();
          ResultSet rslt=stmt.executeQuery(sql);
          
               while(rslt.next()){
                   User fi = new User();
                   fi.setNama(rslt.getString("nama"));
                   fi.setPassword(rslt.getString("pass"));
                   lis.add(fi);   
               }
          
        }catch(Exception err){
            err.printStackTrace();
        }
         
        return lis;
    }

    public void insert(User b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getUsername());
            statement.setString(2, b.getPassword());
            statement.setString(3, b.getNama());
            statement.executeUpdate();
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(User b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(update);
            statement.setString(1, b.getUsername());
            statement.setString(2, b.getPassword());
            statement.setString(3, b.getNama());
            statement.setInt(4, b.getId());
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(delete);

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<User> getALL() {
        List<User> lb = null;
        try {
            lb = new ArrayList<User>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                User b = new User();
                b.setId(rs.getInt("id"));
                b.setUsername(rs.getString("username"));
                b.setPassword(rs.getString("password"));
                b.setNama(rs.getString("nama"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }

    public List<User> getCariNama(String nama) {
        List<User> lb = null;
        try {
            lb = new ArrayList<User>();
            PreparedStatement st = connection.prepareStatement(carinama);
            st.setString(1, "%" + nama + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User b = new User();
                b.setId(rs.getInt("id"));
                b.setUsername(rs.getString("username"));
                b.setPassword(rs.getString("password"));
                b.setNama(rs.getString("nama"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lb;
    }
}
