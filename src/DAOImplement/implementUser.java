/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAOImplement;

import DAOImplement.*;
import java.util.List;
import model.*;
/**
 *
 * @author ASUS
 */
public interface implementUser {
    
    public void insert(User b);

    public void update(User b);

    public void delete(int id);

    public List<User> getALL();

    public List<User> getCariNama(String nama);
    
    public List<User>prosesLogin(String nama, String pass);
}